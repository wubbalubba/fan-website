(function($){
	"use strict"; // Start of use strict

    /*Globals*/
    var activated = {
        products: false
    }
    var mailBarrier = false;
    var s;
    /*Globals*/

    $(".preload > img").preload();

    // Smooth scrolling using jQuery easing
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 48)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });

    // Closes responsive menu when a link is clicked
    $('.navbar-collapse>ul>li>a').click(function() {
        $('.navbar-collapse').collapse('hide');
    });

    /*Smooth scrollbar*/
    	window.goToElement = function(query) {
            if (!query)
                s.scrollTo(0, 0, 600);
            else
                s.scrollIntoView($(query)[0], { offsetTop: $("nav.navbar").height() });
        }
    /*Smooth scrollbar*/

    /*Mail*/
	    window.validate = function() {
	        var textarea = $("#simple-textarea");
	        var body = textarea[0].value;
	        var from_mail = textarea.parent().prev().prev().children("input")[0].value;

	        if (mailBarrier) {
	            $("#simpleMailModalBarrier").modal();
	            return;
	        }

	        if (!body || !from_mail)
	            $("#simpleMailModalWarn").modal();
	        else
	            $("#simpleMailModal").modal();

	    }
  		window.sendMail = function() {

            mailBarrier = true;
            setTimeout(function() {
                mailBarrier = false;
            }, 30000)

            var textarea = $("#simple-textarea");
            var body = textarea[0].value;
            var cellphone = textarea.parent().prev().children("input")[0].value;
            var from_mail = textarea.parent().prev().prev().children("input")[0].value;
            var name = textarea.parent().prev().prev().prev().children("input")[0].value;

            var spaced = '\nMail -' + from_mail + '\n' +
                '\nNombre -' + name + '\n' +
                '\nTeléfono -' + cellphone + '\n' +
                '\nMensaje - ' + body;


            if (!name) name = "Anónimo";
            if (!cellphone) cellphone = "No especificado";
            jQuery.ajax({
                type: "POST",
                url: 'http://narodriguez.com/es/mail.php',
                dataType: 'json',
                data: { message: spaced, subject: $(".chosen.price-product").children("h2").text() }
            });

            $("#simpleMailModalSuccess").modal();
        }
    /*Mail*/

    /*Products*/
    	window.selectProduct = function(message, i) {
            $(".price-product").each(function(index) {
                if (index == i)
                    $(this).addClass("chosen");
                else
                    $(this).removeClass("chosen");
            });
        }
    /*Products*/

    /*Fixes and lazyload*/
	    if ($("html").width() < 768) {
	        console.log("Setting automatic flipping flippers...");
	        setInterval(function() {
	            $(".flip-container").each(function() {
	                $(this).toggleClass("flippity");
	            });
	        }, 5000);
	    }

	    $(".lazyload").each(function(index) {
	        var div = $(this),
	            img, timeout;
	        var source = div.css("background-image").split('url')[1];
	        
	        if(div.css("background-image") == "none")
	        	return;

	        source = source.substring(2, source.length - 2) + "";

	        if (source.indexOf('http://www.fandev.co/img') == -1)
	            source = "http://www.fandev.co/img" + source.split("img")[1];

	        div.css("background-image", "none");
	        div.css("background-color", "black");

	        img = new Image();
	        img.onload = function() {            
	            div.css("background-image", (div.hasClass("masthead") ? "linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), " : "") + "url(" + source + ")");
	        }
	        img.src = source;
	    });
    /*Fixes and lazyload*/

    setTimeout(function(){
	    $(".makeHeightEqualToWidth").each(function() {
	        $(this).height($(this).width());
	    });
    }, 3000);

    function checkScroll() {
    	var nav = $("#mainNav");
    	if (nav.offset().top > 200) {
            nav.addClass("navbar-shrink");
        } else {
            nav.removeClass("navbar-shrink");
        }
    }
    $(window).scroll(checkScroll);
    $("html, body").scroll(checkScroll);

    var Scrollbar = window.Scrollbar;
    s = Scrollbar.init($(".scrollbar")[0]);
    s.addListener(function(status) {
        var nav = $("#mainNav");

        if (status.offset.y > 150) {
            nav.addClass("navbar-shrink");
        } else {
            nav.removeClass("navbar-shrink");
        }
        
        var features = $("section.features");
        if (features.offset().top < nav.height()) { 
            nav.addClass("secondary");
        } else {
        	nav.removeClass("secondary");
        }
    });

    /*Raise the curtains*/   
    new WOW().init();
    $('body').addClass('loaded');
    /*Raise the curtains*/
})(jQuery);