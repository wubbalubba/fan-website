var express = require('express');
var fs = require('fs');
var app = express();

app.use(express.static(__dirname));

app.get('/index.html', function (req, res) {
    fs.readFile("index.html","utf8",function(err, data){
        res.send(data);
    });
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});